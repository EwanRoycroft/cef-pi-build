#!/bin/bash
export CEF_USE_GN=1
export GYP_DEFINES="target_arch=arm arm_float_abi=hard"
export GN_DEFINES="use_jumbo_build=true proprietary_codecs=true ffmpeg_branding=Chrome is_debug=false is_official_build=true use_sysroot=true use_allocator=none symbol_level=0 arm_float_abi=hard"

./cef_create_projects.sh
