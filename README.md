Helper files for CEF build on RPi
=================================

See https://confluence.dev.bbc.co.uk/display/PLATFORMEX/Building+Chromium+Embedded+Framework+for+the+Raspberry+Pi

Original article: https://medium.com/@tejohnso/chromium-embedded-framework-on-the-raspberry-pi-b91b01f51c9
